# Auto_Test 自动化框架

gitee地址：https://gitee.com/tianlei99727202/Auto_Test



### 设计思路:
1、config：

    1.1 分为 path_conf（路径相关配置）+ setting（项目相关配置），具体内容在脚本中有标注

2、database

    2.1 case_yaml: 存放测试用例数据的yaml脚本，对应testcase目录中的测试用例脚本
        将用例脚本 .py替换为 .yml ,从而进行对应用例的数据读取，编写时注意文件命名的一致性

    2.2 locator_yaml: 存放页面操作时对应的元素定位yaml文件，对应pageobj目录，
        将页面操作封装脚本 .py 替换为 .yml 后做到页面动作点击元素位置的准确性，编写时注意文件命名的一致性
3、diver

    存放浏览器驱动目录，目前仅支持chrome，注意浏览器驱动与浏览器版本一致
    chrome指定版本下载：https://www.chromedownloads.net/chrome64win-stable/1215.html
    chrome执行版本驱动：https://npm.taobao.org/mirrors/chromedriver/

4、allure报告等第三方

    使用第三方包Allure，使测试报告更加美观
    下载地址：https://repo.maven.apache.org/maven2/io/qameta/allure/allure-commandline/

5、pageobj
    
    页面动作封装，如登陆操作封装.py ，添加房间动作封装.py 等。
    页面动作所点击的元素位置通过database.locator_yaml中存放

6、public
    
    公共方法的封装，如数据库的操作相关、yaml数据读取相关、浏览器页面操作动作的封装如：打开页面、刷新、点击等

7、testcase

    测试用例集目录，每一个测试脚本按照一套测业务动作来编写，与pageobj相对应。如：登陆、创建房间等
    （测试用例的意义在于保证业务流程，数据流转，不需要考虑反向用例，如：正确用户名密码登陆，不需考虑错误密码登陆）
....


### 项目框架设计图：
```
-- 目前如下图，后续添加Django相关，做成平台化
```


### 项目架构详情：

![](https://gitee.com/tianlei99727202/AutoTest/raw/master/Readme_img/frame1.png)




# 开始使用

### 开始准备


```python
# 安装所需的依赖环境(阿里源安装 * 操作系统中必须有python3, 推荐python3.8或者更高版本)

pip install -r requirements.txt https://mirrors.aliyun.com/pypi/simple  

# 安装配置Allure(官网下载解压包)

解压到allure-commandline文件夹中,把 allure-commandline/bin 加入到环境变量

打开控制台输入:  allure --version   

出来版本代表安装成功

# 确认浏览器版本和driver版本的对应（可写一个简单的调用试一下）

下载浏览器驱动，与浏览器版本相对应

# 运行(run.py 文件即可)  (看run。py中详描述，运行参数怎么写)

python3 run.py
或
pytest ./testcase/web_demo (运行该目录下所有符合条件的测试用例）

```

### 2使用说明

1 本架构元素定位、数据测试用例数据存放依赖为yaml文件 

2 使用前需要对 读取yaml函数(yaml_data.py) ，yaml对应说明仔细阅读 文件内代码处有注释！

3 web-base.py 为 web函数封装 已经封装了功能代码 可以仔细阅读注释来完成页面功能！

6 安装requirements.txt依赖:（交旧版本的包可进行升级安装）
```pip install -r requirements.txt```

7 生成requirements.txt文件:
```pip freeze > requirements.txt```

8 目前浏览器支持 windos(谷歌)， 其它浏览器暂未联调（后续添加）！

9、使用前确保driver可用，可写driver.Chromedriver()代码进行调试
# 更新日志

2021-10-07 -搭建框架文件，但未补充文件内容

2021-11-19 -联调read_data.py(目前打开yml文件时有问题)

2021-11-30 -清除Sonar问题

2021-12-06 -添加讯问指挥项目登陆、房间创建用例，优化其他脚本

