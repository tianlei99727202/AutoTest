#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""========================================================
@author: 田磊
@project: AutoRebuild
@file: xtgl_fjpz_xj.py
@function:
@time: 2021/12/2 11:30
==========================================================="""
import os

from public.common import logger
from public.web_base import WebBase

'''
系统管理 - 房间配置 -新建房间
'''

yamlfile = os.path.basename(__file__).replace('py', 'yml')  # 获取当前目运行文件 并替换为 yml 后缀


class BuildRoom(WebBase):

    def click_xtgl(self):
        """
        点击系统管理模块
        :return:
        """

        logger.debug("点击系统管理模块")
        self.web_exe(yamlfile, 'click_xtgl')

    def click_fjpz(self):
        """
        点击系统管理 - 房间配置模块
        :return:
        """

        logger.debug("点击房间配置模块")
        self.web_exe(yamlfile, 'click_fjpz')

    def click_xj_fj(self):
        """
        点击系统管理 - 房间配置模块 -新建房间按钮
        :return:
        """

        logger.debug("点击新建房间按钮")
        self.web_exe(yamlfile, 'click_xj_fj')

    def input_fjname(self, fjname):
        """
        点击系统管理 - 房间配置模块 -新建房间按钮 - 房间名称文本框并输入
        :return:
        """

        logger.debug(f"点击房间名称文本框并输入:{fjname}")
        self.web_exe(yamlfile, 'input_fjname', text=fjname)

    def click_fj_type(self):
        """
        点击系统管理 - 房间配置模块 -新建房间按钮 - 房间类型文本框
        :return:
        """

        logger.debug("点击房间类型文本框")
        self.web_exe(yamlfile, 'click_fj_type')

    def click_fj_type_list(self, info):
        """
        点击系统管理 - 房间配置模块 -新建房间按钮 - 房间类型 - 下拉框中的房间类型A
        :return:
        """

        logger.debug(f"点击下拉框中的房间类型{info}")
        self.web_exe(yamlfile, 'click_fj_type_list')

    def click_fj_number(self):
        """
        点击系统管理 - 房间配置模块 -新建房间按钮 - 房间号
        :return:
        """

        logger.debug("点击房间号展开房间号选择下拉框")
        self.web_exe(yamlfile, 'input_fj_number')

    def click_fj_number_list(self):
        """
        点击系统管理 - 房间配置模块 -新建房间按钮 - 房间号 - 下拉框中第一个索引的房间号
        :return:
        """

        logger.debug(f"点击下拉框中的第一个索引的房间号")
        self.web_exe(yamlfile, 'click_fj_number_list')

    def click_add_equipment(self):
        """
        点击系统管理 - 房间配置模块 -新建房间按钮 - 添加设备
        :return:
        """

        logger.debug("点击添加房间设备")
        self.web_exe(yamlfile, 'click_add_equipment')

    def input_equipmentID(self, equipmentID):
        """
        点击输入系统管理 - 房间配置模块 -新建房间按钮 - 添加设备 - 房间设备ID
        :return:
        """

        logger.debug(f"点击房间设备ID文本框并输入:{equipmentID}")
        self.web_exe(yamlfile, 'input_equipmentID', text=equipmentID)

    def input_channelID(self, channelID):
        """
        点击输入系统管理 - 房间配置模块 -新建房间按钮 - 添加设备 - 房间信道ID
        :return:
        """

        logger.debug(f"点击房间信道ID文本框并输入:{channelID}")
        self.web_exe(yamlfile, 'input_channelID', text=channelID)

    def click_view_text(self):
        """
        点击系统管理 - 房间配置模块 -新建房间按钮 - 添加设备 - 视角文本框
        :return:
        """

        logger.debug("点击视角文本框")
        self.web_exe(yamlfile, 'click_view_text')

    def click_view_list(self, info):
        """
        点击系统管理 - 房间配置模块 -新建房间按钮 - 添加设备 - 视角下拉框中的房间视角A
        :return:
        """

        logger.debug(f"点击房间视角下拉框并选择视角：{info}")
        self.web_exe(yamlfile, 'click_view_list')

    def input_view_name(self, viewName):
        """
        点击输入系统管理 - 房间配置模块 -新建房间按钮 - 添加设备 - 房间信道ID
        :return:
        """

        logger.debug(f"点击房间信道ID文本框并输入:{viewName}")
        self.web_exe(yamlfile, 'input_view_name', text=viewName)

    def click_enter(self):
        """
        点击系统管理 - 房间配置模块 -新建房间按钮 - 房间类型文本框
        :return:
        """

        logger.debug("点击确定按钮保存新增房间配置")
        self.web_exe(yamlfile, 'click_enter')
