#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""========================================================
@author: 田磊
@project: AutoRebuild
@file: bagl_scdcz_xj.py
@function:
@time: 2021/12/2 10:43
==========================================================="""
import os

from public.common import logger
from public.web_base import WebBase

''''
办案管理-审查调查组-新建
'''
yamlfile = os.path.basename(__file__).replace('py', 'yml')  # 获取当前目运行文件 并替换为 yml 后缀


class BuildDcz(WebBase):

    def click_bagl(self):
        """
        点击办案管理模块
        :return:
        """

        logger.info("点击办案管理模块")
        self.web_exe(yamlfile, 'click_bagl')

    def click_scdcz(self):
        """
        点击办案管理 - 审查调查组模块
        :return:
        """

        logger.debug("点击 审查调查组模块")
        self.web_exe(yamlfile, 'click_scdcz')

    def click_xj_scdcz(self):
        """
        点击办案管理 - 审查调查组模块 - 新建审调查组
        :return:
        """

        logger.debug("点击 新建审查调查组按钮 ")
        self.web_exe(yamlfile, 'click_xj_scdcz')

    def click_xj_scdcz_dczname(self):
        """
        点击办案管理 - 审查调查组模块 - 新建审调查组 - 调查组名称文本框
        :return:
        """

        logger.debug("点击 调查组名称文本框 ")
        self.web_exe(yamlfile, 'click_xj_scdcz_dczname')

    def input_dczname(self, content):
        """
        输入调查组名称
        :param content: 输入内容
        :return:
        """

        logger.debug("输入 调查组名称:", f'{content}')
        self.web_exe(yamlfile, 'input_dczname', text=content)

    def input_badwname(self, content):
        """
        输入办案单位名称
        :param content: 输入办案单位名称
        :return:
        """

        logger.debug("输入 办案单位名称:", f'{content}')
        self.web_exe(yamlfile, 'input_dczname', text=content)

    def click_first_badwname(self, info):
        """
        点击下拉框展示的第一个办案单位名称
        :return:
        """

        logger.debug("点击下拉框展示的第一个办案单位名称: ", f'{info}')
        self.web_exe(yamlfile, 'click_first_badwname')

    def input_babmname(self, content):
        """
        输入办案部门名称
        :param content: 输入办案单位名称
        :return:
        """

        logger.debug("输入办案部门名称:", f'{content}')
        self.web_exe(yamlfile, 'input_babmname', text=content)

    def click_first_babmname(self, info):
        """
        点击下拉框展示的第一个办案单位名称
        :return:
        """

        logger.debug("点击下拉框展示的第一个办案单位名称: ", f'{info}')
        self.web_exe(yamlfile, 'click_first_babmname')
