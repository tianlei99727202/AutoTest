#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""========================================================
@author: 田磊
@project: Auto_Test
@file: login.py
@function:
@time: 2021/10/15 13:39
==========================================================="""
import os
import sys

sys.path.append(os.pardir)
from public.common import logger
from public.web_base import WebBase

yamlfile = os.path.basename(__file__).replace('py', 'yml')  # 获取当前目运行文件 并替换为 yml 后缀

'''
pageobj  对应 /database/locator_yaml 操作页面
'''


class Login(WebBase):

    def input_username(self, username):
        """
        输入用户名
        :param username: 输入内容
        :return:
        """

        logger.debug("正在输入用户名:", f'{username}')
        self.web_exe(yamlfile, 'input_username', text=username)

    def input_password(self, password):
        """
        输入密码
        :param password: 输入内容
        :return:
        """

        logger.debug("正在输入登陆密码:", f'{password}')
        self.web_exe(yamlfile, 'input_password', text=password)

    def click_login_button(self):
        """
        点击登陆按钮
        :return:
        """
        logger.debug("点击登录按钮")
        self.web_exe(yamlfile, 'click_login_button')
