#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""========================================================
@author: TianLei
@project: pythonProject
@file: db.py
@function:数据库相关
@time: 2021/9/30 17:20
==========================================================="""
import pymysql
from typing import TypeVar
import os
import sys

sys.path.append(os.path.dirname(os.path.dirname(__file__)))

from config.setting import mysql
from public.common import logger

T = TypeVar('T')  # 可以是任何类型。


class Mysql:
    """
    mysql 操作类  demo  Mysql.select('SELECT * FROM `case`')
    """

    @classmethod
    def connMysql(cls):
        """
        Mysql 连接
        :return:  str  Mysql连接串
        """
        try:
            conn = pymysql.connect(**mysql)
            return conn
        except Exception as e:
            logger.error(f'Mysql客户端连接失败! {e}')

    @classmethod
    def select(cls, sql):
        """
        SQL 操作   "select * from  `case`"
        :param sql:  str sql
        :return:  tupe
        """
        try:
            conn = cls.connMysql()
            cur = conn.cursor()
            cur.execute(sql)
            select_data = cur.fetchall()
            cur.close()
            conn.close()
            return select_data
        except Exception as e:
            logger.error(f'执行Mysql sql错误{e}')
