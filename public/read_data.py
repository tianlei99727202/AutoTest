#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""========================================================
@author: TianLei
@project: pythonProject
@file: read_data.py
@function:数据读取-Yaml读取、Excel读取、Fake随机数据
@time: 2021/9/30 17:21
==========================================================="""
import os
import sys

sys.path.append(os.path.dirname(os.path.dirname(__file__)))

from typing import List, Tuple
import yaml
from faker import Factory
from config.path_conf import case_yaml_dir, locator_yaml_dir
from public.common import ErrorExcep, logger

fake = Factory().create('zh_CN')


# 读取yaml数据
class GetCaseYaml:
    """
    步骤数据 locator_yaml
    获取测试用例 locatorYaml数据类
    """

    def __init__(self, yaml_name: str, case_name: str) -> None:
        """
        :param yaml_name:  yaml 文件名称
        :param case_name:  用列名称 对应 yaml 用列
        """
        self.modelname = yaml_name  # 模块名称 对应yaml 文件名
        self.yaml_name = yaml_name  # yaml 文件名称 拼接后的路径
        self.case_name = case_name  # 用列名称 对应 yaml 用列

        if case_name.startswith('test'):
            self.file_path = os.path.join(case_yaml_dir, f'{self.yaml_name}')
            logger.info("测试用例数据yaml文件地址为："f'{self.file_path}')
        else:
            self.file_path = os.path.join(locator_yaml_dir, f'{self.yaml_name}')
            logger.info("测试用例定位yaml文件地址为："f'{self.file_path}')

    @property
    def open_yaml(self):
        """
        读取yaml文件
        :return: dict
        """
        try:
            with open(self.file_path, encoding='utf-8') as f:
                data = yaml.load(f, Loader=yaml.FullLoader)
                f.close()
                return data
        except UnicodeDecodeError:
            with open(self.file_path, encoding='GBK') as f:
                data = yaml.load(f, Loader=yaml.FullLoader)
                f.close()
                return data
        except Exception:
            logger.error(' 打开yaml文件失败 ')

    def get_yaml(self):
        """
        返回yaml文件数据
        :return: dict
        """
        yaml_data = self.open_yaml
        if yaml_data is not None:
            return yaml_data[1:]  # 返回用列数据不包含 - model : login 部分 从列表1位置索引
        else:
            logger.error('返回yaml文件中的数据失败,文件内容为空')
            raise Exception

    def get_current_data(self):
        """
        返回 yaml 当前用列的所有数据
        :return: dict
        """
        yaml_List = self.get_yaml()
        for yaml_data in yaml_List:
            # 如果用列等于当前 用列就返回
            if yaml_data.get('casename') == self.case_name:
                return yaml_data
        return "casename 不存在！"

    def count_test_data(self):
        """
        统计 yaml  data 测试数据的条数
        :return:
        """
        yaml_List = self.get_yaml()
        for yaml_data in yaml_List:
            # 如果用列等于当前 用列就返回
            if yaml_data.get('casename') == self.case_name:
                return len(yaml_data.get('testdata'))
        return "casename 不存在！"

    def data_Count(self):
        """
        统计 data  数据条数
        :return:
        """
        return self.count_test_data()

    def get_param(self, value: str) -> str:
        """
        获取 yaml用列参数
        :param value:  传递参数值
        :return:
        """

        yaml_List = self.get_yaml()
        for yaml_data in yaml_List:
            # 如果用列等于当前 用列就返回
            if yaml_data.get('casename') == self.case_name:
                return yaml_data.get(value)
        return "casename 不存在！"

    def get_set(self, index: int, vaule: str):
        """
        获取 set 用列步骤数据

        :param index: 列表索引位置
        :param vaule:  参数值
        :return:
        """
        # 如果读取redis 就从redis获取数据 否则从yaml获取

        data_List = self.get_yaml()
        if index < self.step_count():
            for data in data_List:
                # 如果用列等于当前 用列就返回
                if data.get('casename') == self.case_name:
                    return data.get('element')[index].get(vaule)
        logger.error(f'{self.case_name}用列只有{self.step_count()}个步骤，你确输入了{index} 步！')
        return None

    @property
    def get_model(self):
        """
        返回yaml
        :return: dict
        """
        data = self.open_yaml
        return data[0].get('model')  #

    @property
    def title(self):
        """
        返回用列 title 标题
        :return: str
        """
        #  读取yaml  并返回
        return self.get_param('title')

    @property
    def precond(self):
        """
        返回用列 precond  前置条件
        :return: str
        """
        # 读取yaml参数后返回值

        return self.get_param('precond')

    @property
    def reqtype(self):
        """
        ** HTTP 接口请求参数
        返回用列 reqtype  请类型
        :return: str
        """
        # 读取yaml
        return self.get_param('reqtype')

    def test_data_values(self):
        """
        读取yaml  测试数据的 values
        :return:  demo [('u1', 'p1', 'i1'), ('u2', 'p2', 'i2'), ('u3', 'p3', 'i3')]
        """

        data_values_list = []
        dataList = self.get_yaml()

        for data in dataList:
            # 如果用列等于当前 用列就返回 并且读取的是 yaml 数据

            if data.get('casename') == self.case_name:
                data_list = data.get('testdata')
                for i in data_list:
                    data_values_list.append(tuple(i.values()))
                return data_values_list

    def test_data_list(self, index: int, agrs: str) -> str:
        """

        返回 用列 测试 data 数据列表
        :param index: 列表的索引位置
        :param agrs: 字段的key  因为测试数据是可变的增加的
        :return:
        """
        dataList = self.get_yaml()
        if index < self.data_Count():

            for data in dataList:
                # 如果用列等于当前 用列就返回 并且读取的是 yaml 数据

                if data.get('casename') == self.case_name:
                    return data.get('testdata')[index].get(agrs)

        logger.error(f'{self.case_name}用列只有{self.data_Count()}条数据，你输入了第{index} 条！')

    def step_count(self) -> int:
        """
        统计 yaml 测试步骤条数
        :return:
        """
        data_List = self.get_yaml()

        if data_List:
            for data in data_List:
                # 如果用列等于当前 用列就返回
                if data.get('casename') == self.case_name:
                    return len(data.get('element'))
        else:
            logger.error('用例不存在！请检查yml文件')
            raise ErrorExcep('用例不存在！请检查文件')

    def test_data(self):
        """
        返回yaml  test_data 全部数据 列表字段
        :return:
        """

        return self.get_current_data().get('testdata')

    def casesteid(self, index: int) -> int:
        """
       返回 用列步骤 casesteid 参数
       """
        return self.get_set(index, 'casesteid')

    def types(self, index: int) -> str:
        """
        返回 用列步骤 types 参数
        """
        return self.get_set(index, 'types')

    def operate(self, index: int) -> str:
        """
        返回 用列步骤 operate 参数
        """
        return self.get_set(index, 'operate')

    def locate(self, index: int) -> str:
        """
        返回 用列步骤 locate 参数
        """
        return self.get_set(index, 'locate')

    def listindex(self, index: int) -> int:
        """
        返回 用列步骤 listindex 参数
        """
        return self.get_set(index, 'listindex')

    def locawait(self, index: int or float) -> int or float:
        """
        返回 用列步骤 locawait 参数
        """
        return self.get_set(index, 'locawait')

    def info(self, index: int) -> str:
        """
        返回 用列步骤 info 参数
        """
        return self.get_set(index, 'info')


# faker 随机数据类
class RandomData:
    """
    基于 faker 封装个人测试信息类
    """

    @property
    def random_name(self):
        """
        随机姓名
        :return: str
        """
        return fake.name()

    @property
    def random_phone_number(self):
        """
        随机手机号码
        :return:  int
        """
        return fake.phone_number(self)

    @property
    def random_email(self):
        """
        随机邮箱
        :return:
        """
        return fake.email(self)

    @property
    def random_job(self):
        """
       随机职位
       :return:
       """
        return fake.job(self)

    @property
    def random_ssn(self):
        """
       随机 省份证信息
       :return:
       """
        return fake.ssn(min_age=18, max_age=90)

    @property
    def random_company(self):
        """
        随机 公司名
        :return:
        """
        return fake.company(self)

    @property
    def random_city(self):
        """
        随机 城市
        :return:  str
        """
        return fake.city_name()

    @property
    def random_province(self):
        """
        随机 省份
        :return:  str
        """
        return fake.province(self)

    @property
    def random_country(self):
        """
        随机 国家
        :return:  str
        """
        return fake.country(self)

    @property
    def random_address(self):
        """
        随机住址信息
        :return:  str
        """
        return fake.address(self)

    @property
    def random_time(self):
        """
        随机时间24H   22:00:00
        :return: str
        """
        return fake.time()

    @property
    def random_year(self):
        """
        随机月份
        :return: str[0] -数字月  str[0] -英文月
        """
        return fake.month(), fake.month_name()

    @property
    def random_month(self):
        """
        随机年份
        :return: str
        """
        return fake.month(self)

    @property
    def random_date_this_month(self):
        """
        随机 本月中的日期时间
        :return: str
        """
        return fake.date_time_this_month(before_now=True, after_now=False, tzinfo=None)

    @property
    def random_date_this_decade(self):
        """
        随机 本年中的日期时间
        :return: str
        """
        return fake.date_time_this_year(before_now=True, after_now=False, tzinfo=None)

    @property
    def random_date_time_this_century(self):
        """
        随机 本世纪中的日期和时间
        :return: str
        """
        return fake.date_time_this_century(before_now=True, after_now=False, tzinfo=None)

    @property
    def random_day_of_week(self):
        """
        随机星期
        :return:  str
        """
        return fake.day_of_week()

    def random_date_of_birth(self, age):
        """
        随机生日
        :param age:  int  需要随机多少岁之内
        :return:  str
        """
        return fake.date_of_birth(tzinfo=None, minimum_age=0, maximum_age=age)


#  快速获取测试数据 *元组 WEB、APP
def reda_pytestdata(yaml_name: str, case_name: str, ) -> List or Tuple:
    """
    * pytest.mark.parametrize()  *此函数只支持在pytest框架内使用
    * 如果配合run函数调用自己在pytest.mark.parametrize() 传入列表 否则其它方法传入字段名
    快速获取测试数据 *元组
    :param yaml_name: yaml 名称
    :param case_name:   用例数据
    :return:
    """
    testdata = GetCaseYaml(yaml_name, case_name).test_data_values()
    print(testdata)
    return testdata


#  快速获取测试数据 *字典 API
def reda_api_casedata(yaml_name, case_name):
    """
    读取测试数据 HTTP 专用  *字典
    :return:
    """
    testdata = GetCaseYaml(yaml_name, case_name)

    return testdata.test_data()
