# -*- coding: utf-8 -*-
"""========================================================
@author: TianLei
@project: pythonProject
@file: api_base.py
@function:浏览器驱动相关内容
@time: 2021/9/30 17:16
==========================================================="""
import os
import sys

# import  xxx，默认情况下python解析器会搜索当前目录,import sys>>sys.path.append(‘../’)来指定位置
sys.path.append(os.path.dirname(os.path.dirname(__file__)))
import time
from loguru import logger
import requests
from selenium import webdriver
from config.path_conf import win_chromedriver
from config.setting import browser_name, project_url
from typing import TypeVar

DAY = time.strftime("%Y-%m-%d", time.localtime(time.time()))
T = TypeVar('T')  # 可以是任何类型。


class WebInit:
    """
    返回浏览器驱动
    """

    def __init__(self):
        self.browser = browser_name.lower()
        self.base_url = project_url

    @staticmethod
    def inspect_url_code(url: str) -> bool:
        """
        判断url 地址正常请求
        """
        try:
            rep = requests.get(url, timeout=5)  # 默认设置5秒超时
            code = rep.status_code
            if code == 200:
                return True
            else:
                return False
        except Exception as e:
            logger.error(f'请求地址异常{e}！！')

    @property
    def url(self):
        logger.debug(f"当前登陆的url为: {self.base_url}")
        return self.base_url

    @url.setter
    def url(self, value):
        self.base_url = value

    @property
    def enable(self) -> T:
        """
        启用模式
        :return:
        """
        return self.setup()

    def browaer_setup_args(self, driver: T) -> T:
        """
        单机浏览器参数设置
        :param driver: driver驱动浏览器
        :return:
        """
        driver.maximize_window()
        driver.get(self.url)
        return driver

    def setup(self) -> T:
        """
        设置单机版 浏览器驱动
        :return:
        """
        # 判断当前系统
        try:
            if self.inspect_url_code(self.url):  # 如果项目地址正常
                current_sys = sys.platform.lower()
                # log_path = os.path.join(log_dir, f'{DAY}firefox.log')

                if current_sys == 'win32':

                    if self.browser == 'chrome':
                        driver = webdriver.Chrome(executable_path=win_chromedriver)
                        return self.browaer_setup_args(driver)
                    else:
                        logger.error(f'windows系统不支持此浏览器: {self.browser}')

                        return False
                else:
                    logger.error(f'当前{current_sys}系统不支持！')
                    return False
            else:
                logger.error('项目地址地址请求异常！！！')
                return False

        except Exception as e:
            logger.error(f'浏览器驱动启动失败 {e}')
            return False
