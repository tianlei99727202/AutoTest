#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""========================================================
@author: TianLei
@project: pythonProject
@file: common.py
@function:通用方法封装-logger日志类、Del_Report报告清理类、img_diff图片对比类、等
@time: 2021/9/30 17:17
==========================================================="""
import os
import sys

sys.path.append(os.path.dirname(os.path.dirname(__file__)))
import re
import shutil
import time
from loguru import logger
from config.path_conf import log_dir, test_allure_dir, test_result_dir, test_screen_dir
from config.setting import level, is_clean_report
from typing import TypeVar

# 可以是任意类型
T = TypeVar('T')


def find_dict(will_find_dist: dict, find_keys: T) -> list or int:
    """
    查询 嵌套字典中的值
    :param will_find_dist:  要查找的字典
    :param find_keys:  要查找的key
    :return:  list
    """

    value_found = []
    if isinstance(will_find_dist, list):  # 含有列表的值处理
        if len(will_find_dist) > 0:
            for now_dist in will_find_dist:
                found = find_dict(now_dist, find_keys)
                if found:
                    value_found.extend(found)
            return value_found

    if not isinstance(will_find_dist, dict):  # 没有字典类型的了
        return 0

    else:  # 查找下一层
        dict_key = will_find_dist.keys()
        for i in dict_key:
            if i == find_keys:
                value_found.append(will_find_dist[i])
            found = find_dict(will_find_dist[i], find_keys)
            if (found):
                value_found.extend(found)

        return value_found


class SetLog:
    """
    日志设置类  使用 logger 请从此logs目录导入
    """

    DAY = time.strftime("%Y-%m-%d", time.localtime(time.time()))

    log_path = os.path.join(log_dir, f'{DAY}_all.log')

    err_log_path = os.path.join(log_dir, f'{DAY}_err.log')

    logger.add(log_path, rotation="00:00", encoding='utf-8')

    logger.add(err_log_path, rotation="00:00", encoding='utf-8', level='DEBUG', )

    # 删去import logger之后自动产生的handler，不删除的话会出现重复输出的现象
    logger.remove()

    # 添加一个可以修改控制的handler
    handler_id = logger.add(sys.stderr, level=level)


# 自定义异常类
class ErrorExcep(Exception):
    """
    自定义异常类
    """

    def __init__(self, message):
        super().__init__(message)


def is_assertion(dicts: T, actual: T) -> None:
    """
    断言参数
    :param dicts: dict 断言参数
    :param actual: 实际结果
    :return:
    """

    if dicts is not None:
        is_assertion_results(actual=actual, expect=dicts[-2], types=dicts[-1])


def is_assertion_results(actual: T, expect: T, types: str) -> bool:
    """
    断言函数
    :param actual: 实际结果
    :param expect:  预期结果
    :param types:  断言类型    ==(等于) !=(不等于) in(包含) notin(不包含)
    :return:
    """
    if isinstance(actual, dict):
        if isinstance(expect, dict):
            actual = find_dict(actual, list(expect)[0])  # 利用字典的键获取断言的值
            expect = list(expect.values())[0]
    if types == '==':
        assert expect == actual
        return True

    elif types == '!=':
        assert expect != actual
        return True

    elif types == 'in':
        assert expect in actual
        return True

    elif types == 'notin':
        assert expect not in actual
        return True

    elif types is not None:
        return False
    else:
        logger.error('输入的类型不支持！！')
        return False


def facename(func: T) -> T:
    """
    获取函数名称 *装饰器
    :param func:
    :return:
    """

    def wrapper(*args, **kwargs):
        name = func.__name__
        return name

    return wrapper


def ymal(*args, **kwargs) -> T:
    """
    装饰器
    获取当前运行文件的py文件并转为yaml
    :param args:
    :param kwargs:
    :return:
    """

    def get_yaml(func):
        def yaml():
            name = func.__name__
            yamlfile = args[0](__file__).name.replace('py', 'yml')
            return yamlfile, name

        return yaml

    return get_yaml


# 获取运行函数名称
def get_run_func_name() -> T:
    """
    获取运行函数名称
    :return:
    """
    try:
        raise Exception
    except:
        exc_info = sys.exc_info()
        traceObj = exc_info[2]
        frameObj = traceObj.tb_frame
        Upframe = frameObj.f_back
        return Upframe.f_code.co_name


# 提取字符中的整数
def extract_str_in_int(string: str) -> list:
    """
    提取字符中的整数
    :param string: 字符串
    :return: list
    """
    findlist = re.findall(r'[1-9]+\.?[0-9]*', string)
    return findlist


# 删除测试报告
class DelReport:

    @staticmethod
    def mkdir(path: str) -> None:
        """
        文件夹不存在就创建
        :param path:
        :return:
        """
        folder = os.path.exists(path)
        if not folder:  # 判断是否存在文件夹如果不存在则创建为文件夹
            os.makedirs(path)
        else:
            pass

    @staticmethod
    def clean_report(filepath: str) -> None:
        """
        清除测试报告文件
        :param filepath:  str  清除路径
        :return:
        """
        del_list = os.listdir(filepath)
        if del_list:
            try:
                for f in del_list:
                    file_path = os.path.join(filepath, f)

                    # 判断是不是文件
                    if os.path.isfile(file_path):
                        if not file_path.endswith('.xml'):  # 不删除.xml文件
                            os.remove(file_path)
                    else:
                        os.path.isdir(file_path)
                        shutil.rmtree(file_path)
            except Exception as e:
                logger.error(e)

    def run_del_report(self, ) -> None:
        """
        执行删除测试报告记录
        :return:
        """
        # 如果为 True 清除 test_allure_dir、 test_json_dir 、test_screen_dir 路径下报告
        if is_clean_report is True:

            try:
                dir_list = [test_allure_dir, test_result_dir, test_screen_dir]
                for dir_demo in dir_list:
                    self.mkdir(dir_demo)
                    self.clean_report(dir_demo)
                logger.info('正在清理历史的测试报告...')
            except Exception as e:
                logger.error(e)

        else:
            logger.warning('清理测试报告功能未启用！')
