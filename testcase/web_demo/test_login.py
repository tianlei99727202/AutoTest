#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""========================================================
@author: 田磊
@project: Auto_rebuild
@file: test_login.py
@function:
@time: 2021/11/19 17:14
==========================================================="""
import os
import sys

sys.path.append(os.path.dirname(os.path.dirname(__file__)))

import pytest
import allure

from public.web_base import AutoRunCase
from public.read_data import reda_pytestdata

'''
测试登陆功能
'''

yamlfile = os.path.basename(__file__).replace('py', 'yml')  # 获取当前目运行文件


class TestLogin:

    @allure.feature("用户登陆")  # 测试用例特性（主要功能模块）
    @allure.story("用户登陆")  # 模块说明
    @allure.title("输入用户名密码并登录")  # 用例标题
    @allure.description('邓志刚登陆')  # 用例描述
    @pytest.mark.test_login  # 用列标记
    @pytest.mark.parametrize('testdata', reda_pytestdata(yamlfile, 'test_login'))  # 测试数据
    @pytest.mark.run(order=1)
    def test_login(self, webDriver, testdata):
        with allure.step('输入用户名密码'):
            login = AutoRunCase(webDriver)
            login.run(yamlfile, 'test_login', test_date=testdata, forwait=1)
