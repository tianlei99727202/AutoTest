#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""========================================================
@author: 田磊
@project: AutoRebuild
@file: test_add_room.py
@function:
@time: 2021/12/2 14:04
==========================================================="""
import os

import allure
import pytest

from pageobj.xtgl_fjpz_xj import BuildRoom
from public.read_data import reda_pytestdata
from public.web_base import AutoRunCase

'''
测试房间添加
'''

yamlfile = os.path.basename(__file__).replace('py', 'yml')  # 获取当前目运行文件


class TestAddRoom:

    @allure.feature("新建房间")  # 测试用例特性（主要功能模块）
    @allure.story("创建留置房间")  # 模块说明
    @allure.title("测试留置房间创建")  # 用例标题
    @allure.description('留置室')  # 用例描述
    @pytest.mark.test_add_room  # 用列标记
    @pytest.mark.parametrize('testdata', reda_pytestdata(yamlfile, 'test_add_room'))  # 测试数据
    @pytest.mark.run(order=2)
    def test_add_room(self, webDriver, testdata):
        addroom = AutoRunCase(webDriver)
        addroom.run(yamlfile, 'test_add_room', test_date=testdata, forwait=1)