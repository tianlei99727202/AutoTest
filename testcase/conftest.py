# -*- coding: utf-8 -*-
"""========================================================
@author: TianLei
@project: pythonProject
@file: api_base.py
@function:pytest共享文件
@time: 2021/9/30 17:16
==========================================================="""
import pytest
from public.driver_init import WebInit


@pytest.fixture(scope='session')
def webDriver():
    wb = WebInit()
    driver = wb.enable
    yield driver
    driver.quit()
