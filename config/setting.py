# -*- coding: utf-8 -*-
"""========================================================
@author: TianLei
@project: pythonProject
@file: api_base.py
@function:项设置相关的配置内容
@time: 2021/9/30 17:16
==========================================================="""
import os
import sys

sys.path.append(os.path.dirname(os.path.dirname(__file__)))

#  通用配置 ---------------------------------------------------------

# 项目的类型：'app' 'web' 'api'
project_type = 'web'

# 是否清除 '测试历史-测试报告结果'  '*'为默认清除
is_clean_report = True

# 日志级别 'INFO' or 'DEBUG'
level = 'DEBUG'

#  web端配置 ---------------------------------------------------------

# 项目url地址
project_url = "http://172.18.21.182:8989/jwywglpt/"

# 选择使用的浏览器 'Chrome' or 'Firefox' or 'IE'
browser_name = "Chrome"

# 平台配置 -讯问指挥项目（Dahua  or Haikang）
platform = "Dahua"

# # 接口端配置 ----------------------------------------------------------
# API_URL = "http://wthrcdn.etouch.cn"  # "https://github.com/" #'"http://192.168.7.101/api"  # 接口地址
#
# TIMEOUT = 5  # 接口请求超时时间 /S
#
# HEADERS = {'Content-Type': 'application/json', }
#  DB相关配置 ---------------------------------------------------------

# mysql数据库信息
mysql = {
    'user': 'root',
    'password': '6789@JKL',
    'port': 6543,
    'host': '172.18.17.128',
    'db': 'db_sxzh'
}
# selenium配置 -------------------------------------------------------

# 显示等待最长时间 /s
implicitly_wait_time = 10

# 显示等待元素出现时,多长时间去检索元素一次 /s
poll_frequency = 0.2