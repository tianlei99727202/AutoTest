#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""========================================================
@author: TianLei
@project: pythonProject
@file: api_base.py
@function:路径相关配置
@time: 2021/9/30 17:16
==========================================================="""
import os
import sys

sys.path.append(os.path.dirname(os.path.dirname(__file__)))

base_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))  # 获取当前脚本所在项目的项目路径

# windows 系统浏览器驱动路径--------------------------------------------------------------
win_chromedriver = os.path.join(base_dir, "driver", "windows", "chromedriver.exe")

# 日志路径-------------------------------------------------------------------------------
log_dir = os.path.join(base_dir, "log")

# 测试用例集路径-------------------------------------------------------------------------
case_dir = os.path.join(base_dir, "testcase", )

# yaml测试用列数据路径-------------------------------------------------------------------
case_yaml_dir = os.path.join(base_dir, "database", "case_yaml", )  # 测试数据
locator_yaml_dir = os.path.join(base_dir, "database", "locator_yaml", )  # 定位数据

# 测试文件路径--------------------------------------------------------------------------
data_file_dir = os.path.join(base_dir, "database", "file")

# 测试用例结果目录----------------------------------------------------------------------
test_result_dir = os.path.join(base_dir, "output", "report_json")

# 测试结果报告目录-----------------------------------------------------------------------
test_allure_dir = os.path.join(base_dir, "output", "report_allure")

# 测试截图目录--------------------------------------------------------------------------
test_screen_dir = os.path.join(base_dir, "output", "report_screen")
